# Python-IDE-with-vim
You might think that your development ENV wouldnt effect much but i am gonna prove you wrong.In this repositorie i am going to share that sweet coconut oil.So lets get star#ted
# Introduction
# Pro's
1)Autocomplete

2)Nerdtree for filenavigation

3)Python mode with syntax check and file icons(dev icons)

4)Syntax check

5)Vim airlines

6)Vim tabline(if you use VIM in terminal)

7)Wonderful UI(with airline and much more goodies)
# Wonderful autocompletion
I use Tabnine and COC.NVIM and no I am not sponsored. COC.NVIM is fast and easy to install. I had wasted a lost of time installing YouCompletMe.So I recommend COC.nvim.You may face some problems after installing such as no autocompleteion.When this happens just type :CocInstall coc-python(https://github.com/neoclide/coc.nvim)

![image](https://github.com/Sreehariqwerty/Python-IDE-with-vim/blob/main/autocomplete.png)

# Code Folding
Code folding enables us to understand long code properly. This come with most Modern IDE's.type za to open the fold while in normal mode.Type zc to close the fold in normal mode.You can also remap it in your init.vim

![image](https://github.com/Sreehariqwerty/Python-IDE-with-vim/blob/main/code%20folding.png)

# Proper syntax error checking and error checking 
This prevents us from breaking our heads by checking whats wrong.This happens because of the wondrful LSP server with the python mode

![image](https://github.com/Sreehariqwerty/Python-IDE-with-vim/blob/main/syntax%20errror.png)

# Nerd Tree - file navigation
Nerd Tree is wonderful for file navigation. Most Vim users are familiar about it.

![image](https://github.com/Sreehariqwerty/Python-IDE-with-vim/blob/main/Nerd%20Tree.png)

# Fuzzy Finder
Telescope is possibly the best fuzzy finder for neovim.Just type :Telescope to the fuzzy finder.
# Vim Airline
Vim airline helps us to differnetiate between insert visual and normal mode by changing the color of the statusline in different modes
you can change the theme of the status line by mentioning
```
let g:airline_theme='<theme>'

```
Where theme can be gruvbox(my favourate)/badwolf or deus theme(nice boi)
For exmaple if you want the status linetheme to be deus
```
let g:airline_theme=deus
```

# With Gruvbox theme for statusline
# In Normal Mode

![image](https://github.com/Sreehariqwerty/Python-IDE-with-vim/blob/main/Nerd%20Tree.png)

# In Visual Mode 

![image](https://github.com/Sreehariqwerty/Python-IDE-with-vim/blob/main/vim%20air.png)

# In Insert Mode

![image](https://github.com/Sreehariqwerty/Python-IDE-with-vim/blob/main/autocomplete.png)

# Installation
This Vim IDE can be installed with Windows and Linux and is under GNU lisence 
# Windows
# Install neovim-qt or neovim-gtk or any other vim GUI.
1)Link for Downloading Neovim-https://github.com/neovim/neovim/releases/

2)Link for Downloading Neovide-https://github.com/Kethku/neovide/blob/main/README.md

# Easier way to install is to dowload scoop and run this


Scoop download-https://scoop.sh/


For neovim
```
scoop install neovim
```
For neovide
```
scoop install neovide
```

# Linux

# For Debian
to install neovim
```
sudo apt-get install neovim

```
or to install neovide

```
sudo apt-get install neovide

```

# For arch 
to install neovim
```
sudo pacman -S install neovim

```
or to install neovide

```
sudo pacman -S install neovide

```
to install neovim
```
sudo apt-get install neovim

```
or to install neovide

```
sudo apt-get install neovide

```

# Refer configuring.md to configure you neovim or neovide into an IDE 
https://github.com/Sreehariqwerty/Python-IDE-with-vim/blob/main/Configuring.md

# Major Plugins used
1)Vim airline

https://github.com/vim-airline/vim-airline

2)Vim airline themes(photos avialable in WIKI)

https://github.com/vim-airline/vim-airline-themes

2)Tabnine

https://www.tabnine.com/install/vim

3)Vim Rainbow

https://github.com/frazrepo/vim-rainbow

4)Vim autoclose brackets

https://github.com/jiangmiao/auto-pairs

